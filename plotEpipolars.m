%% plot n epipolar lines
function [] = plotEpipolars(Im1, Im2, fa, fb, F, n)
	% @param Im1, Im2 -- two image matrix
	% @param fa, fb -- corresponding features of Im1 and Im2
	% @param F -- fundamental matrix between camera1 and camera2
	% @param n -- number of epipolar lines to plot
	
	if(size(fa) ~= size(fb))
		error('size of fa and fb must be same');
	end
	if(size(fa,1) ~= 3)
		error('fa and fb must be in homogeneous form [wx;wy;w]');
	end

	% convert to form [x,y,1]
	fa = bsxfun(@rdivide, fa, fa(end,:));
	fb = bsxfun(@rdivide, fb, fb(end,:));

	indices = randperm(size(fa,2),n);

	
	x1 = fa(:,indices);
	x2 = fb(:,indices);

	% these contain lines corresponding to point pairs
	l1 = F'*x2;
	l2 = F *x1;

	% to matlab polynomial form
	l1_poly = bsxfun(@rdivide, -l1([1 3],:), l1(2,:));
	l2_poly = bsxfun(@rdivide, -l2([1 3],:), l2(2,:));

	figure(1);
	imshow(Im1);
	figure(2);
	imshow(Im2);

	for i=1:size(l1,2)
	    x = linspace(1, size(Im1,2));
	    y = polyval(l1_poly(:,i)', x);
	    figure(1);
	    hold on;
	    plot(x,y,'-b',x1(1,i),x1(2,i),'*r')

	    y = polyval(l2_poly(:,i)', x);
	    figure(2);
	    hold on;
	    plot(x,y,'-b',x2(1,i),x2(2,i),'*r')
	end

	% to save the images: print("-dpngalpha", "filename.png")
end