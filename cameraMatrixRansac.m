function [camera_matrix, max_inliers] = cameraMatrixRansac(f, threeD)
    %% reconstruct camera matrix from feature points and their corresponding 3D coordinates
    %% uses RANSAC to get the best fit
    %% @param f -- features points (in homogeneous image coordinate frame)
    %% @param threeD -- 3D coordinates corresponding to feature points
    %%               -- in homogeneous world coordinate frame
    %% @output camera_matrix -- the P matrix

    if(size(f,2) ~= size(threeD,2))
        error('number of points in f and X should be same');
    elseif(size(f,1) ~= 3)
        error('each point in f must be of form [wx;wy;w]');
    elseif(size(threeD,1) ~= 4)
        error('each point in threeD must be of form [wX;wY;wZ;w]');
    end

    f = bsxfun(@rdivide, f, f(end,:));
    threeD = bsxfun(@rdivide, threeD, threeD(end,:));

    % threshold (allowable error)
    threshold = 25;
    % start with infinte number of iterations, and reduce iterations based on proportion of inliers and outliers
    N = inf;
    % assume we get 50% outliers every time
    e = .5;
    % number of samples
    s = 5;
    % confidence probability (estimated probability of inliers in the set)
    p = .6;

    iteration_count = 1;
    max_inliers = [];
    best_camera_matrix = [];
    
    while iteration_count<1000 % fixed no. of iterations
    %while N > iteration_count % dynamic no. of iterations
        samples = randperm(size(f,2),s);
        
        x = f(:,samples);
        X = threeD(:, samples);
        P = cameraMatrix(x, X);
        % reproject 3D point using cameraMatrix
        reproj = P * threeD;
        reproj = bsxfun(@rdivide, reproj, reproj(end,:));
        
        difference = reproj(1:2,:) - f(1:2,:);
        inliers = abs(difference) < threshold;
        inliers = inliers(1,:) & inliers(2,:);
        inliers = find(inliers);

        if(length(inliers) > length(max_inliers))
            max_inliers = inliers;
            camera_matrix = P;
        end

        %disp(sprintf('iteration count: %d, inliers: %d\n',iteration_count, length(inliers)));
        if(length(inliers)==0)
            N=inf;
        else
            e = 1 - length(inliers)/size(f,2);
            N = log(1-p)/log(1-((1-e)^s));
            iteration_count = iteration_count + 1;
        end
    end
    disp(sprintf('iteration count: %d\n',iteration_count));
end
