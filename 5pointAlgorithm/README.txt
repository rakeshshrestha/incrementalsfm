% Zhaopeng Cui 2015.2

This folder contains an implementation of Nister's 5 point algorithm[1] for computing essential matrices as well as an demo of how to compute E robustly through RANSAC.

1. calibrated_fivepoint_helper.c and calibrated_fivepoint.m were written by Henrik Stewenius.

2. compute_E_ransac.m utilizes calibrated_fivepoint to compute E robustly by using RANSAC.

3. Demo.m shows how to use compute_E_ransac. 

4. "corrs.txt" contains matching correspondences between two images. The format for this file is as follow,
<number of matches> <Image1's index> <Image2's index>
<x1> <y1> <x2> <y2>
...

5. "calibration.txt" contains the intrinsic information for each camera. Its format is as follow,
<f0> <cx0> <cy0> 
<f1> <cx1> <cy1>  
...
***Here f_x and f_y are considered to be equal, and you can modify by yourself for the situation when f_x is not the same with f_y.

Note:
Before you run, you should first compile calibrated_fivepoint_helper.c by running compile.m.
 
 
Reference
[1] David Nist��r. An efficient solution to the five-point relative pose problem. IEEE Transactions on Pattern Analysis and Machine Intelligence(PAMI), 26(6):756�C777, 2004.