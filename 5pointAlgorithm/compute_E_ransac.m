% Compute the Essential matrix robustly using 5-point algorithm

function [E_best, R, t, inliers] = compute_E_ransac(l_pts, r_pts, ransac_t, K_l, K_r, thr)

% Function [E_best, R, t] = compute_E_ransac(l_pts, r_pts, ransac_t, K_l, K_r, thr)
% Zhaopeng Cui 2015.2
%
% Inputs:
% l_pts, r_pts -- 3*N matrix, which contains the feature coordinates in the
% left / right image;
% ransac_t -- The parameter controls the times of performing RANSAC;
% K_l, K_r -- 3*3 matrix, the intrinsic matrix of left / right image;
% thr -- The threshold for judging the inliers;
% inliers -- indices of inliers (updated by Rakesh)
% M -- 3D coordinates of feature points
%
%
% Outputs:
% E_best -- The computed essential matrix;
% R -- The relative rotation matrix between two images decomposed from E;
% t -- The relative translation direction between two images decomposed from E. 
% E_best = [t]x R, here [t]x is the Skew-symmetric matrix of vector t.

    E_best = [];
    num_pts = size(l_pts,2);
    thr2 = thr^2;

   % Compute the inverse of K_r and K_l
    K_l_inv = inv(K_l);
    K_r_inv = inv(K_r);
    
    % Compute the normalized points
    l_pts_norm = K_l_inv * l_pts;
    r_pts_norm = K_r_inv * r_pts;
    
    max_num_inlers = -1;

    %% updated by Rakesh for dynamic number of iterations
    % start with infinte number of iterations, and reduce iterations based on proportion of inliers and outliers
    N = inf;
    % assume we get 50% outliers every time
    e = .5;
    % we take five samples in each iteration 
    s = 5; 
    % confidence probability (estimated probability of inliers in the set)
    p = .8;
    % whether we can break RANSAC loop or not
    break_ransac = false;

    %% updated by Rakesh for finding inliers among the features (zero means no, one means yes)
    max_inliers = zeros(num_pts,1);

    % Loop for RANSAC
    for i = 1: ransac_t
        indices = randperm(num_pts,5);
        l_pts_inner = l_pts_norm(:, indices);
        r_pts_inner = r_pts_norm(:, indices);
        
        Evec   = calibrated_fivepoint(l_pts_inner, r_pts_inner);
        
        for j=1:size(Evec,2)
            E = reshape(Evec(:,j),3,3)';     
            F = K_r_inv' * E * K_l_inv;
            
            % Compute the inliers
            num_inliers = 0;
            %% updated by Rakesh for finding inliers among the features (zero means no, one means yes)
            is_inliers = zeros(num_pts,1);
            for k = 1:num_pts
                 err = compute_sampon_error(l_pts(:,k), r_pts(:,k), F);
                 if err< thr2
                     num_inliers = num_inliers+1;
                     is_inliers(k) = 1;
                 end
            end
           
            % Update E
            if max_num_inlers < num_inliers
                E_best = E;
                max_num_inlers = num_inliers;
                %% updated by Rakesh for finding inliers among the features
                max_inliers = is_inliers;
            end
            %% updated by Rakesh for dynamic number of iterations
            % disp(sprintf('%d/%d\n',num_inliers,num_pts));
            e = 1 - num_inliers/num_pts;
            N = log(1-p)/log(1-((1-e)^s));
            if(i>N && i>15) % go atleast 15 rounds
                break_ransac = true;
                break; 
            end
    
        end % end of  j=1:size(Evec,2)

        if(break_ransac)
            fprintf('No of ransac iterations: %d\n',i);
            break;
        end
        
        
    end
    fprintf('Number of inliers: %d out of %d \n\n',max_num_inlers,num_pts);
    [R, t] = Get_Rt_from_E(E_best,l_pts_norm,r_pts_norm);
    %% updated by Rakesh for finding inliers among the features
    inliers = find(max_inliers==1);

end

function err = compute_sampon_error(l_pt, r_pt, F)
    F_l = F*l_pt;
    F_r = F'*r_pt;
    err = (r_pt'*F_l)^2*(1/(F_l(1)^2+F_l(2)^2) + 1/(F_r(1)^2+F_r(2)^2) );
    %err = (r_pt'*F_l)^2/((F_l(1)^2+F_l(2)^2) + (F_r(1)^2+F_r(2)^2) );
end