% function expects a 3x3 R and a 3x1 T
% R and T should be such that P'=R(P-T)
% input_points and base_points should be 3xN matrix

function [output]= depth( R, T, input_points, base_points)

[row, col]=size(input_points);

for i=1:col
    
    A=[];
    p1=input_points(:,i);
    p2=base_points(:, i);
    
    p1=p1/norm(p1);
    p2=p2/norm(p2);
    
    pp1=p1;
    pp2=R'*p2;
    pp3=cross(pp1, pp2);
    pp3=pp3/norm(pp3);
    
    A=cat(2, pp1, -1*pp2);
    A=cat(2, A, pp3);
    
    result=pinv(A)*T;

    if i==1
        output=result;
    else
        output=cat(2, output, result);
    end;
    
    
end;
