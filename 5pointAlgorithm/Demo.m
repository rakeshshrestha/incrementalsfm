%%% Demo for computing the essential matrix using 5 point algorithm %%%

%Set some parameter first
ransac_round = 500;
thr = 4.0;
file_calibration = 'calibration.txt';
file_corrs = 'corrs.txt';

% Load calibration information
if exist(file_calibration,'file')
    cam_intrinsics = dlmread(file_calibration);
else
    fprintf('ERROR: unable to open calibration file\n');
    return;
end

% Load image correspondences and compute E
fid = fopen(file_corrs);

if fid>0
    while(~feof(fid))
        str = fgetl(fid);
        if ~isempty(str)
            %read corrs
            [nMatches, v1, v2] =  strread(str,'%d %d %d');
            corrs = textscan(fid, '%f %f %f %f',nMatches);      
            v1 = v1+1;
            v2 = v2+1;

            % Set intrisinc matrix
            K1 = diag([cam_intrinsics(v1,1), cam_intrinsics(v1,1), 1]);
            K1(1,3) =  cam_intrinsics(v1,2);
            K1(2,3) =  cam_intrinsics(v1,3);
            K2 = diag([cam_intrinsics(v2,1), cam_intrinsics(v2,1), 1]);
            K2(1,3) =  cam_intrinsics(v2,2);
            K2(2,3) =  cam_intrinsics(v2,3);

            [E, R, t]  = compute_E_ransac([corrs{1}';corrs{2}'; ones(1,nMatches)], [corrs{3}';corrs{4}'; ones(1,nMatches)], ransac_round, K1, K2, thr);
            disp([R;t']);

            str=fgetl(fid);
        end
    end
    fclose(fid);
else
    fprintf('ERROR: unable to open correspondence file\n');
end

