function [] = writeBal(filename, point_cloud, unique_features, contributors, transformation, K, no_of_images)
	no_of_observations = 0;
	% normalize the features
	K_inv = inv(K);
	for i = 1:length(unique_features)
		unique_features{i} = K_inv*[unique_features{i}; ones(1,size(unique_features{i},2))];
		unique_features{i} = -unique_features{i}(1:2,:);
		no_of_observations = no_of_observations + size(unique_features{i},2);
	end
	% bal header
	dlmwrite(filename, ...
			[no_of_images, length(point_cloud), no_of_observations], ...
			'delimiter', ' ', 'append','off');

	% write the observations
	observations = zeros(no_of_observations, 4);
	observation_index = 1;
	for i=1:length(point_cloud)
		contributing_images = contributors{i};
	    for j = 1:length(contributing_images)
	        image_index = contributing_images(j);
	        f = unique_features{i}(:,j);
	        observations(observation_index, :) = [image_index-1, i-1, f(1), f(2)];
	        observation_index = observation_index + 1;
	    end
	end

	dlmwrite(filename, observations , 'delimiter', ' ', 'append','on');

	% write camera parameters
	camera_parameters = zeros(no_of_images, 9);
	for i=1:no_of_images
		T = transformation{i};
		R = T(1:3,1:3);
		t = T(1:3, end)';
		r = rodrigues(R)';
		camera_parameters(i,:) = [r t 1 0 0];
	end
	dlmwrite(filename, camera_parameters , 'delimiter', ' ', 'append','on');

	% write points
	threeD = zeros(3, length(point_cloud));
	for i = 1:length(point_cloud)
	    threeD(:,i) = point_cloud{i};
	end
	points = reshape(threeD, [3*length(point_cloud),1]);
	dlmwrite(filename, points , 'delimiter', ' ', 'append','on');
end
