## About
This project implements structure-from-motion (SfM) for generating 3D point cloud from a number of static scene captured by a moving camera. 

## Files and Folders
MainSFM.m is the main script and other files are helper modules.The folders within this repository contain sample images for reconstruction.

## Language
The project was done in Octave. It should be compatible with MATLAB as well. If any problem arises when running on MATLAB, feel free to contact me.

## Note
This project requires that you have VLFeat library already setup before running its scripts.