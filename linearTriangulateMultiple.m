%% finding x,y,z coordinates one corresponding feature using triangulation
function [threeD] = linearTriangulateMultiple(P_all, f)
	% @param P_all: camera matrices for all images stacked vertically
	% @param f: corresponding features from all camera (only one feature allowed)
	% @output threeD: 3D coordinates of corresponding feature

	if(size(P_all,2) ~= 4)
		error('all P must have four columns')
	end
	if(mod(size(P_all,1),3) ~= 0)
		error('all P matrices should have 3 rows')
	end
	if(size(P_all,1)/3 ~= size(f,2))
		error('no of P matrices and no of features must be same');
	end
	if(size(f,1) ~= 3)
		error('f be in homogeneous form [wx;wy;w]');
	end

	% convert to form [x,y,1]
	f = bsxfun(@rdivide, f, f(end,:));
	A = zeros(2*size(f,2),4);
	threeD = zeros(3,1);
	for i=1:size(f,2)
		P = P_all(3*i-2:3*i, :);
	    x = f(1:2,i);
	    A(2*i-1:2*i,:) = [...
	            P(3,:)*x(1) - P(1,:);
	            P(3,:)*x(2) - P(2,:);
			];
	end
	[~,~,V] = svd(A);
	threeD = V(1:3,end)/V(4,end);

	%% minimization of reprojection error
	%triangulationError(threeD, P_all, f)
	errorFunction = @(X) triangulationError(X, P_all, f);
	threeD = lsqnonlin(errorFunction, threeD);
	% options.Jacobian = 'on';
	% threeD = lsqnonlin(errorFunction, threeD, [], [], options);

end

function [err] = triangulationError(threeD, P_all, f)
	normalizeHomogeneous = @(x) bsxfun(@rdivide, x, x(end,:));
	reprojectionError = @(x,X,P) sqrt(sum(((normalizeHomogeneous(x) - normalizeHomogeneous(reshape(P,[3,4])*X)).^2)))';

	err = zeros(size(f,2),1);
	
	for i=1:size(f,2)
		P = P_all(3*i-2:3*i, :);
	    x = f(:,i);
	    err(i) = reprojectionError(x,[threeD;1], P);
	end
end

% function [err,J] = triangulationError(threeD, P_all, f)
% 	normalizeHomogeneous = @(x) bsxfun(@rdivide, x, x(end,:));
% 	reprojectionError = @(x,X,P) sqrt(sum(((normalizeHomogeneous(x) - normalizeHomogeneous(reshape(P,[3,4])*X)).^2)))';

% 	X = sym('X',[3,1]);

% 	err = zeros(size(f,2),1);
% 	if (nargout>1)
% 		disp('here');
% 		J = zeros(size(f,2),3);
% 	end
% 	for i=1:size(f,2)
% 		P = P_all(3*i-2:3*i, :);
% 	    x = f(:,i);
% 	    err_sym = reprojectionError(x,[X;1], P);
% 	    err(i) = double(subs(err_sym,X,threeD))
% 	    if (nargout>1)
% 	    	disp('here');
% 	    	J_sym = jacobian(err_sym, X);
% 			J(i,:) = double(subs(J_sym,X,threeD))
% 		end
% 	end
% end
