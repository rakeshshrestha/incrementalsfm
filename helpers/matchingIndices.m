%% find the columns in M matching vector v
function [matching_indices] = matchingIndices(M,v)
    % if x and y are apart only by this threshold, we assume they match
    feature_match_threshold = .00000001;

    difference = bsxfun(@minus, M, v);
    matching = abs(difference)<feature_match_threshold;
    matching = matching(1,:) & matching(2,:);
    matching_indices = find(matching);
end
