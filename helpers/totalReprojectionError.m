%% find reprojection error from every 3D-2D correspondence
%% find total reprojection error (from every point in cloud to its projections on different image)
function [reproj_error] = totalReprojectionError(threeD, unique_features, contributors, P)
    reproj_error = 0;
    for i = 1:length(unique_features)
        contributing_images = contributors{i};
        for j = 1:length(contributing_images)
            image_index = contributing_images(j);
            feature = unique_features{i}(:,j);
            projection = P{image_index} * [threeD(:,i); 1];
            projection = projection(1:2,:) / projection(3,:);
            reproj_error = sum(sum((feature - projection).^2));
        end
    end
end