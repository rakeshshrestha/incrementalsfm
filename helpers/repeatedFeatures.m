%% find the features in fb matching with fa
function [indices_map] = repeatedFeatures(fa, fb)
    % features of fb matching with fa (0 means no match)
    % indices_map(3) = 603 means fb(:,3) matches with fa(:,603)
    % indices_map(4) = 0 means fb(:,4) matches with no feature in fa
    
    indices_map = zeros(1,size(fb,2));
    for i = 1:size(fb,2)
        matching_indices = matchingIndices(fa(1:2,:), fb(1:2,i));
        if(length(matching_indices))
            indices_map(i) = matching_indices(1);
        else
            indices_map(i) = 0;
        end
    end
end
