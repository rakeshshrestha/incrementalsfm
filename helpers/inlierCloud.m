function [cloud3, unique_features3, contributors3] = inlierCloud(cloud1, cloud2, unique_features, contributors)
	threshold = .15;

	cloud3 = [];
	unique_features3 = [];
	contributors3 = [];
	j = 1;
	for i=1:length(cloud1)
		difference = abs(cloud1{i} - cloud2{i});
		is_inlier = difference < threshold;
		if(is_inlier(1) && is_inlier(2))
			cloud3{j} = cloud2{i};
			unique_features3{j} = unique_features{i};
			contributors3{j} = contributors{i};
			j = j + 1;
		end
	end
end