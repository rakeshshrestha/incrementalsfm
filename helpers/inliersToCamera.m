%% find the inliers among the features
function [inliers] = inliersToCamera(Pa, Pb, fa, fb)
    % variation upto 1 pixels allowed (in both x and y) to be considered inlier
    threshold = 1;

    threeD = linearTriangulate(Pa, Pb, fa, fb);
    threeD = [threeD; ones(1,size(threeD,2))];
    reproj_a = Pa * threeD;
    reproj_b = Pb * threeD;
    reproj_a = bsxfun(@rdivide, reproj_a, reproj_a(end,:));
    reproj_b = bsxfun(@rdivide, reproj_b, reproj_b(end,:));

    difference1 = abs(fa(1:2,:) - reproj_a(1:2,:));
    difference2 = abs(fb(1:2,:) - reproj_b(1:2,:));
    inliers1 = difference1 < threshold;
    inliers2 = difference2 < threshold;
    inliers = inliers1(1,:) & inliers1(2,:) & inliers2(1,:) & inliers2(2,:);
    inliers = find(inliers);
                 
end
