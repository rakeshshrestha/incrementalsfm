function [f_out] = featuresToStandardFrame(f_in, im_size)
	% convert from matlab array notation to standard image frame
	
	% center = (im_size(1:2) + [1 1])/2.;
	% f_out = bsxfun(@minus,f_in, [center(2);center(1);0]);

	f_out = f_in;
	
	% f_out(1,:) = bsxfun(@minus, f_in(1,:), center(2));
	% f_out(2,:) = bsxfun(@minus, f_in(2,:), center(1));
end