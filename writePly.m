function [ply_matrix] = writePly(filename, Im, point_cloud, unique_features, contributors)
	%% write the point cloud from each image pair to ply file
	% this matrix contains point location in space and corresponding RGB values in alternating rows
	ply_matrix = zeros(2*length(point_cloud),3);
	for i=1:length(point_cloud)
	    RGB = [];
	    contributing_images = contributors{i};
	    for j = 1:length(contributing_images)
	        image_index = contributing_images(j);
	        pixel = round(unique_features{i}(:,j));
	        temp_RGB = double(Im{image_index}(pixel(2), pixel(1), :));
	        RGB = [RGB; temp_RGB(1,1,1) temp_RGB(1,1,2) temp_RGB(1,1,3)];
	    end
	    RGB = round(mean(RGB));
	    ply_matrix(2*i-1,:) = point_cloud{i}';
	    ply_matrix(2*i,:) = RGB;
	end
	
	ply_content = "ply\nformat ascii 1.0\nelement face 0\nproperty list uchar int vertex_indices\n";
	ply_content = strcat(ply_content,['element vertex ' num2str(uint64(size(ply_matrix,1)/2)) "\n"]);
	ply_content = strcat(ply_content,"property float x\nproperty float y\nproperty float z\nproperty uchar diffuse_red\nproperty uchar diffuse_green\nproperty uchar diffuse_blue\nend_header\n");

	fid = fopen(filename,'w');
	fwrite(fid, ply_content);
	fclose(fid);
	dlmwrite(filename, ply_matrix, 'delimiter', ' ', 'append','on');
end
