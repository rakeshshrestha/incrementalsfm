function [ply_matrix] = writePlyMatrix(filename, ply_matrix)
		
	ply_content = "ply\nformat ascii 1.0\nelement face 0\nproperty list uchar int vertex_indices\n";
	ply_content = strcat(ply_content,['element vertex ' num2str(uint64(size(ply_matrix,1)/2)) "\n"]);
	ply_content = strcat(ply_content,"property float x\nproperty float y\nproperty float z\nproperty uchar diffuse_red\nproperty uchar diffuse_green\nproperty uchar diffuse_blue\nend_header\n");

	fid = fopen(filename,'w');
	fwrite(fid, ply_content);
	fclose(fid);
	dlmwrite(filename, ply_matrix, 'delimiter', ' ', 'append','on');
end
