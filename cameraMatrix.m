function [camera_matrix] = cameraMatrix(x, X)
	%% reconstruct camera matrix from feature points and their corresponding 3D coordinates
	%% @param x -- 2D features points (in homogeneous image coordinate frame)
	%% @param X -- 3D coordinates corresponding to feature points x
	%% 			-- in homogeneous world coordinate frame
	%% @output camera_matrix -- the P matrix
	
	if(size(x,2) ~= size(X,2))
		error('number of points in x and X should be same');
	elseif(size(x,1) ~= 3)
		error('each point in x must be of form [wx;wy;w]');
	elseif(size(X,1) ~= 4)
		error('each point in X must be of form [wX;wY;wZ;w]');
	end

	normalizeHomogeneous = @(x) bsxfun(@rdivide, x, x(end,:));
	x = normalizeHomogeneous(x);
	X = normalizeHomogeneous(X);

	%%	find camera matrix using DLT
	% TODO: the resultant solution from SVD has reduntant final two elements. Fix it
	A = zeros(12, 13);
	for i=1:size(x,2)
	    Xt = X(:,i)';
	    A(3*i-2:3*i,:) = [...
	                        Xt           zeros(1,4)   zeros(1,4)   -x(1,i);
	                        zeros(1,4)   Xt           zeros(1,4)   -x(2,i);
	                        zeros(1,4)   zeros(1,4)   Xt           -1
	                    ];
	end
	[~,~,V] = svd(A);
	camera_matrix = V(1:(end-1),end)/V(end,end);
	camera_matrix = reshape(camera_matrix, [4,3])';

	%% find camera matrix using linear equation solutions (AX = B => X = inv(A)*B) for each row of P
	% if(size(X,2) ~= 4)
	% 	error('need exactly four points for camera matrix estimation');
	% end
	% A =	[...
 %            X'           zeros(4,4)   zeros(4,4);
 %            zeros(4,4)   X'           zeros(4,4);
 %            zeros(4,4)   zeros(4,4)   X'
 %        ];
	% camera_matrix = inv(A)*reshape(x',[12,1]);
	% camera_matrix = reshape(camera_matrix, [4,3])';

	%% minimization of reprojection error
	reprojectionError = @(P) sqrt(sum(((normalizeHomogeneous(x) - normalizeHomogeneous(reshape(P,[3,4])*X)).^2)))';

	
	camera_matrix = lsqnonlin(reprojectionError, reshape(camera_matrix,[12,1]));
	camera_matrix = reshape(camera_matrix, [3,4]);

	

end