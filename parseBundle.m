function [point_cloud, transformation, f,k] = parseBundle(point_file, camera_file)
	point_cloud  = dlmread(point_file, ' ')';
	point_cloud = point_cloud(1:3,:);
	camera_param = dlmread(camera_file, ' ');
	camera_param = camera_param(:,1:9);

	f = zeros(size(camera_param,1), 1);
	k = zeros(size(camera_param,1), 2);
	for i = 1:size(camera_param,1)
		R = camera_param(i,1:3);
		R = rodrigues(R);
		t = camera_param(i,4:6)';
		transformation{i} = [R t; zeros(1,3) 1];
		f(i) = camera_param(i,7);
		k(i,:) = camera_param(i,8:9);
	end
end