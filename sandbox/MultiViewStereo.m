%% this script just dumps all the point clouds found by using two adjacent images in single space
close all;
clear all;

%Set some parameter first
ransac_round = 500;
thr = 4.0;

%% read the images
Im{1} = imread('../fountain-P11/0000.jpg');
Im{2} = imread('../fountain-P11/0001.jpg');
Im{3} = imread('../fountain-P11/0002.jpg');
Im{4} = imread('../fountain-P11/0003.jpg');
Im{5} = imread('../fountain-P11/0004.jpg');
Im{6} = imread('../fountain-P11/0005.jpg');
Im{7} = imread('../fountain-P11/0006.jpg');
Im{8} = imread('../fountain-P11/0007.jpg');
Im{9} = imread('../fountain-P11/0008.jpg');
Im{10} = imread('../fountain-P11/0009.jpg');
Im{11} = imread('../fountain-P11/0010.jpg');


%% resize image (apply if you are running out of memory)
% for i = 1:length(Im)
%   Im{i} = imresize(Im{i},.5);
% end

%% intrisic matrix (given)
K = [ ...
        1077.9  0       594.0
        0       1077.9  393.3
        0       0       1   ...
    ];

% this is transformation from image frame to world frame
% the first image frame is considered as world frame
transformation{1} = [eye(3) zeros(3,1);zeros(1,3) 1];
% the camera matrix for different camera (first camera is reference)
P{1} = K * [eye(3) zeros(3,1)];% equivalent to K * [I|0] * transformation{1}
% this matrix contains point location in space and corresponding RGB values in alternating rows
ply_matrix = [];

for image_index=2:length(Im)
    %% get corresponding features (in homogeneous frame)
    [fa, fb] = getCorrespondingFeatures(Im{image_index-1}, Im{image_index});
    fa = [fa; ones(1,size(fa,2))];
    fb = [fb; ones(1,size(fb,2))];

    %% compute the essential matrix (and extrinsic parameters)
    [E, R, t, inliers]  = compute_E_ransac(fa, fb, ransac_round, K, K, thr);
    % the only features I'm interest in plotting are inliers
    fa = fa(:,inliers);
    fb = fb(:,inliers);
    
    %% compute the fundamental matrix
    %F = inv(K)'*E*inv(K);
    %% plot some epipolar lines
    % plotEpipolars(Im{1}, Im{2}, fa(:,inliers), fb(:,inliers), F, 5);

    % transformation from image_index-1 frame to image_frame
    T = [R t; zeros(1,3) 1];
    % transformation from world frame to image frame
    transformation{image_index} = T * transformation{image_index-1};
    % P = K * [I|0] * transformation
    P{image_index} = K * [eye(3) zeros(3,1)] * transformation{image_index};
    
    %% finding x,y,z coordinates of corresponding features using triangulation
    threeD = linearTriangulate(P{image_index-1}, P{image_index}, fa, fb);
    %% check reprojection error
    % threeD = [threeD; ones(1,size(threeD,2))];
    % reproj1 = P1*threeD;
    % reproj2 = P2*threeD;
    % % make the last homogeneous coordinate 1
    % reproj1 = bsxfun(@rdivide, reproj1, reproj1(end,:));
    % reproj2 = bsxfun(@rdivide, reproj2, reproj2(end,:));

    %% getting the point cloud
    % TODO: see if you can interpolate pixels instead of rounding
    fa_pixels = round(fa(1:2,:));
    fb_pixels = round(fb(1:2,:));

    for i = 1:size(fa,2)
        pixel1 = fa_pixels(:,i);
        pixel2 = fb_pixels(:,i);
        RGB = (double(Im{image_index-1}(pixel1(2),pixel1(1),:)) + double(Im{image_index}(pixel2(2),pixel2(1),:))) / 2.;
        RGB = round(RGB);
        ply_matrix = [ply_matrix;
                    threeD(1:3,i)'];
        ply_matrix = [ply_matrix;
                    RGB(1,1,1) RGB(1,1,2) RGB(1,1,3)];                    
    end
end
writePly('outputMVS.ply', ply_matrix);