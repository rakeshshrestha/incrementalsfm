function [reproj_error_vector] = reprojectionErrorVectorRaw(pin, unique_features, contributors, K, no_of_images)
    % the first P matrix is default
    P{1} = K * [eye(3) zeros(3,1)];
    % the transfomation matrices of images (other than the first)
    if(size(pin,1)==1 || size(pin,2)==1)
        pin = reshape(pin,[3,length(pin)/3]);
    end
    
    for i = 2:no_of_images
        j = i-1;
        T = [pin(:,j*4-3:j*4); [zeros(1,3) 1]];
        P{i} = K * [eye(3) zeros(3,1)] * T;
    end

    threeD = pin(:,4*(no_of_images-1)+1:end);

    no_of_error_elements = 0;
    for i=1:length(contributors)
        no_of_error_elements = no_of_error_elements + length(contributors{i});
    end
    reproj_error_vector = zeros(no_of_error_elements,1);
    error_count = 1;
    for i = 1:length(unique_features)
        contributing_images = contributors{i};
        for j = 1:length(contributing_images)
            image_index = contributing_images(j);
            feature = unique_features{i}(:,j);
            projection = P{image_index} * [threeD(:,i); 1];
            projection = projection(1:2,:) / projection(3,:);
            reproj_error = sqrt(sum((feature - projection).^2));
            reproj_error_vector(error_count) = reproj_error;
            error_count = error_count + 1;
        end
    end
    
end
