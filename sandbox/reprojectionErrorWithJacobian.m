function [reproj_error_vector, J] = reprojectionErrorWithJacobian(pin, unique_features, contributors, K, no_of_images)
	pin_sym = sym('pin_sym', [size(pin,1)*size(pin,2), 1]);
	reproj_error_sym = reprojectionErrorVectorRaw(pin_sym, unique_features, contributors, K, no_of_images);
	reproj_error_vector = subs(reproj_error_sym, pin_sym, pin);
	J = jacobian(reproj_error_sym, pin_sym);
	J = subs(J, pin_sym, pin);

	fprintf('%f\n',sum(reproj_error_vector));
end