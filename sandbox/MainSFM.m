%% this script just dumps only the unique features of the scene on point cloud (ply file)
close all;
clear all;

%%%%%%%%%%%%%%%%%%%%%%%%
%% helper functions
%%%%%%%%%%%%%%%%%%%%%%%%

%% find the features in fb matching with fa
function [indices_map] = repeatedFeatures(fa, fb)
    % features of fb matching with fa (0 means no match)
    % indices_map(3) = 603 means fb(:,3) matches with fa(:,603)
    % indices_map(4) = 0 means fb(:,4) matches with no feature in fa
    
    indices_map = zeros(1,size(fb,2));
    for i = 1:size(fb,2)
        matching_indices = matchingIndices(fa(1:2,:), fb(1:2,i));
        if(length(matching_indices))
            indices_map(i) = matching_indices(1);
        else
            indices_map(i) = 0;
        end
    end
end

%% find the columns in M matching vector v
function [matching_indices] = matchingIndices(M,v)
    % if x and y are apart only by this threshold, we assume they match
    feature_match_threshold = .00000001;

    difference = bsxfun(@minus, M, v);
    matching = abs(difference)<feature_match_threshold;
    matching = matching(1,:) & matching(2,:);
    matching_indices = find(matching);
end

%Set some parameter first
ransac_round = 500;
thr = 4.0;
% number of past images to check for repeated features
no_of_past_images = 2;

%% read the images
Im{1} = imread('../fountain-P11/0000.jpg');
Im{2} = imread('../fountain-P11/0001.jpg');
Im{3} = imread('../fountain-P11/0002.jpg');
% Im{4} = imread('../fountain-P11/0003.jpg');
% Im{5} = imread('../fountain-P11/0004.jpg');
% Im{6} = imread('../fountain-P11/0005.jpg');
% Im{7} = imread('../fountain-P11/0006.jpg');
% Im{8} = imread('../fountain-P11/0007.jpg');
% Im{9} = imread('../fountain-P11/0008.jpg');
% Im{10} = imread('../fountain-P11/0009.jpg');
% Im{11} = imread('../fountain-P11/0010.jpg');


%% resize image (apply if you are running out of memory)
% for i = 1:length(Im)
%   Im{i} = imresize(Im{i},.5);
% end

%% intrisic matrix (given)
K = [ ...
        1077.9  0       594.0
        0       1077.9  393.3
        0       0       1   ...
    ];

% this is transformation from image frame to world frame
% the first image frame is considered as world frame
transformation{1} = [eye(3) zeros(3,1);zeros(1,3) 1];

% the camera matrix for different camera (first camera is reference)
P{1} = K * [eye(3) zeros(3,1)];% equivalent to K * [I|0] * transformation{1}

% this matrix contains point location in space and corresponding RGB values in alternating rows
ply_matrix = [];

% Corresponding features of images 1 and 2 stacked vertically
% these features are not present in other indices of unique_features (theoritically), hence unique
% unique_features{1}->4XN matrix, 
unique_features{1} = [];

% 3XN matrix - 3D coordinates corresponding to the unique_features
% point_cloud{1} -> 3D coordinates of corresponding unique_features between image 1 and 2
point_cloud{1} = [];

% rgb_stack{1}{1} -> kX3 matrix (k is the number of overlapping pixel RGB values)
% rgb_stack{1}{1} -> RGB values from all images of the first corresponding feature between images 1 and 2
% may come from more than 2 images (if they are indeed the same feature)
rgb_stack{1}{1} = [];

for image_index=2:length(Im)
    %% get corresponding features between current and previous image (in homogeneous frame)
    [fa, fb] = getCorrespondingFeatures(Im{image_index-1}, Im{image_index});
    fa = [fa; ones(1,size(fa,2))];
    fb = [fb; ones(1,size(fb,2))];

    %% compute the essential matrix (and extrinsic parameters)
    [E, R, t, inliers]  = compute_E_ransac(fa, fb, ransac_round, K, K, thr);
    % the only features I'm interest in plotting are inliers
    fa = fa(:,inliers);
    fb = fb(:,inliers);
        
    %% compute the fundamental matrix
    %F = inv(K)'*E*inv(K);
    %% plot some epipolar lines
    % plotEpipolars(Im{1}, Im{2}, fa(:,inliers), fb(:,inliers), F, 5);

    % transformation from image_index-1 frame to image_frame
    T = [R t; zeros(1,3) 1];
    % transformation from world frame to image frame
    transformation{image_index} = T * transformation{image_index-1};
    % P = K * [I|0] * transformation
    P{image_index} = K * [eye(3) zeros(3,1)] * transformation{image_index};
    
    if(image_index==2)
        % for the first pair of images, all the features are unique
        unique_features{image_index-1} = [fa(1:2,:); fb(1:2,:)];
    else
        %% TODO: not only for 1 previous image, use correspondence of the current image with a number of previous image
        %% would need to find SIFT matching for those pair not already computed

        %% add points to the cloud only if unique feature found
        past_features = unique_features{image_index-2}(3:4,:);
        past_features = [past_features; ones(1,size(past_features,2))];
        indices_map = repeatedFeatures(past_features,fa);
        matching_indices = find(indices_map);
        non_matching_indices = find(~indices_map);
        % add the RGB value of the matching feature to old rgb_stack (for later averaging)
        for i=1:length(matching_indices)
            feature_index_present = matching_indices(i);
            feature_index_previous = indices_map(feature_index_present);
            pixel = round(fb(:,feature_index_present));
            RGB = Im{image_index}(pixel(2), pixel(1),:);
            rgb_stack{image_index-2}{feature_index_previous} = [...
                                                            rgb_stack{image_index-2}{feature_index_previous};
                                                            RGB(1,1,1) RGB(1,1,2) RGB(1,1,3)
                                                            ];  
        end
        % add non matching feature (i.e unique feature)
        unique_features{image_index-1} = [...
                                            fa(1:2,non_matching_indices);
                                            fb(1:2,non_matching_indices)
                                        ];
    end
    no_of_unique_features = size(unique_features{image_index-1},2);
    %% finding x,y,z coordinates of corresponding features using triangulation
    threeD = linearTriangulate( P{image_index-1}, 
                                P{image_index}, 
                                [unique_features{image_index-1}(1:2,:); ones(1,no_of_unique_features)],
                                [unique_features{image_index-1}(3:4,:); ones(1,no_of_unique_features)]
                            );
    point_cloud{image_index-1} = threeD(1:3,:);
    for i=1:no_of_unique_features
        pixel1 = round(unique_features{image_index-1}(1:2,i));
        pixel2 = round(unique_features{image_index-1}(3:4,i));
        RGB1 = Im{image_index-1}(pixel1(2),pixel1(1),:);
        RGB2 = Im{image_index}(pixel2(2),pixel2(1),:);
        rgb_stack{image_index-1}{i} = [RGB1(1,1,1) RGB1(1,1,2) RGB1(1,1,3);
                                        RGB2(1,1,1) RGB2(1,1,2) RGB2(1,1,3)];
    end
end

%% write the point cloud from each image pair to ply file
no_of_points_in_cloud = 0;
for i=2:length(Im)
    no_of_points_in_cloud = no_of_points_in_cloud + size(unique_features{i-1},2);
end
% this matrix contains point location in space and corresponding RGB values in alternating rows
ply_matrix = zeros(2*no_of_points_in_cloud,3);
point_count = 1;
for image_index=2:length(Im)
    for i = 1:size(unique_features{image_index-1},2)
        pixel1 = round(unique_features{image_index-1}(1:2,i));
        pixel2 = round(unique_features{image_index-1}(3:4,i));

        RGB = mean(rgb_stack{image_index-1}{i});
        RGB = round(RGB);
        ply_matrix(2*point_count-1,:) = point_cloud{image_index-1}(:,i)';
        ply_matrix(2*point_count,:) = RGB;
        point_count = point_count + 1;
    end
end
writePly('output.ply', ply_matrix);