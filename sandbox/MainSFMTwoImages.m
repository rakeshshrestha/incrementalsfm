close all;
clear all;

%Set some parameter first
ransac_round = 500;
thr = 4.0;

%% read the images
Im{1} = imread('../fountain-P11/0000.jpg');
Im{2} = imread('../fountain-P11/0001.jpg');

%% resize image (because I am running out of memory)
% for i = 1:length(Im)
%   Im{i} = imresize(Im{i},.5);
% end

%% intrisic matrix (given)
K = [ ...
        1077.9  0       594.0
        0       1077.9  393.3
        0       0       1   ...
    ];

%% get corresponding features (in homogeneous frame)
[fa, fb] = getCorrespondingFeatures(Im{1}, Im{2});
fa = [fa; ones(1,size(fa,2))];
fb = [fb; ones(1,size(fb,2))];

%% compute the essential matrix (and extrinsic parameters)
[E, R, t, inliers]  = compute_E_ransac(fa, fb, ransac_round, K, K, thr);

%% plot some epipolar lines on inliers
fa_inliers = fa(:, inliers);
fb_inliers = fb(:, inliers);
indices = randperm(size(fa_inliers,2),5);

F = inv(K)'*E*inv(K);
x1 = fa_inliers(:,indices);
x2 = fb_inliers(:,indices);

% these contain lines corresponding to point pairs
l1 = F'*x2;
l2 = F *x1;

% to matlab polynomial form
l1_poly = bsxfun(@rdivide, -l1([1 3],:), l1(2,:));
l2_poly = bsxfun(@rdivide, -l2([1 3],:), l2(2,:));

figure(1);
imshow(Im{1});
figure(2);
imshow(Im{2});

for i=1:size(l1,2)
    x = linspace(1, size(Im{1},2));
    y = polyval(l1_poly(:,i)', x);
    figure(1);
    hold on;
    plot(x,y,'-b',x1(1,i),x1(2,i),'*r')

    y = polyval(l2_poly(:,i)', x);
    figure(2);
    hold on;
    plot(x,y,'-b',x2(1,i),x2(2,i),'*r')
end

% to save the images: print("-dpngalpha", "filename.png")



% the first camera is supposed to be the reference frame
extrinsic1 = [eye(3) zeros(3,1)];
extrinsic2 = [R t];
P1 = K * extrinsic1;
P2 = K * extrinsic2;

%% finding P2 from E (this has already been done during compute_E_ransac)
% [U, S, V] = svd(E);
% % S should be diag([1,1,0])
% U = U*mean([S(1,1) S(2,2)]);
% W = [0 -1 0; 1 0 0; 0 0 1];
% P2_1 = [U*W*V',  U(:,3)];
% P2_2 = [U*W*V', -U(:,3)];
% P2_3 = [U*W'*V',  U(:,3)];
% P2_4 = [U*W'*V', -U(:,3)];
% % camera center (second camera)
% C = -R'*t;

%% finding x,y,z coordinates of corresponding features using triangulation
threeD = zeros(3,size(fa,2));
for i=1:size(fa,2)
    x = fa(1:2,i);
    xp = fb(1:2,i);
    A = [...
            P1(3,:)*x(1) - P1(1,:);
            P1(3,:)*x(2) - P1(2,:);
            P2(3,:)*xp(1) - P2(1,:);
            P2(3,:)*xp(2) - P2(2,:)...
        ];

    [~,~,V] = svd(A);
    threeD(:,i) = V(1:3,end)/V(4,end);

    % only y, xp, yp are correct (x is not correct)
    % A = A(2:4,:);
    % threeD(:,i) = inv(A(:,1:3)) * -A(:,4);
end

% TODO: see if you can interpolate pixels instead of rounding
fa_pixels = round(fa(1:2,:));
fb_pixels = round(fb(1:2,:));
ply_content = "ply\nformat ascii 1.0\nelement face 0\nproperty list uchar int vertex_indices\n";
ply_content = strcat(ply_content,['element vertex ' num2str(length(inliers)) "\n"]);
ply_content = strcat(ply_content,"property float x\nproperty float y\nproperty float z\nproperty uchar diffuse_red\nproperty uchar diffuse_green\nproperty uchar diffuse_blue\nend_header\n");
% ply_matrix has alternately x,y,z coordinates and RGB values for each point pair

%% writing only the inliers in ply file
% ply_content = "ply\nformat ascii 1.0\nelement face 0\nproperty list uchar int vertex_indices\n";
% ply_content = strcat(ply_content,['element vertex ' num2str(length(inliers)) "\n"]);
% ply_content = strcat(ply_content,"property float x\nproperty float y\nproperty float z\nproperty uchar diffuse_red\nproperty uchar diffuse_green\nproperty uchar diffuse_blue\nend_header\n");
% ply_matrix = zeros(length(inliers)*2, 3);
% for i = 1:length(inliers)
%     pixel1 = fa_pixels(:,inliers(i));
%     pixel2 = fb_pixels(:,inliers(i));
%     RGB = (double(Im{1}(pixel1(2),pixel1(1),:)) + double(Im{2}(pixel2(2),pixel2(1),:))) / 2.;
%     RGB = uint8(RGB);
%     ply_matrix(2*i-1, :) = threeD(:,inliers(i))';
%     ply_matrix(2*i, :) = [RGB(1,1,1) RGB(1,1,2) RGB(1,1,3)];
% end

%% writing all the corresponding points in ply file
ply_content = "ply\nformat ascii 1.0\nelement face 0\nproperty list uchar int vertex_indices\n";
ply_content = strcat(ply_content,['element vertex ' num2str(size(fa,2)) "\n"]);
ply_content = strcat(ply_content,"property float x\nproperty float y\nproperty float z\nproperty uchar diffuse_red\nproperty uchar diffuse_green\nproperty uchar diffuse_blue\nend_header\n");
ply_matrix = zeros(size(fa,2)*2, 3);
for i = 1:size(fa,2)
    pixel1 = fa_pixels(:,i);
    pixel2 = fb_pixels(:,i);
    RGB = (double(Im{1}(pixel1(2),pixel1(1),:)) + double(Im{2}(pixel2(2),pixel2(1),:))) / 2.;
    RGB = uint8(RGB);
    ply_matrix(2*i-1, :) = threeD(:,i)';
    ply_matrix(2*i, :) = [RGB(1,1,1) RGB(1,1,2) RGB(1,1,3)];
end

fid = fopen('output2.ply','w');
fwrite(fid, ply_content);
fclose(fid);
dlmwrite('output2.ply', ply_matrix, 'delimiter', ' ', 'append','on');

% check reprojection error
threeD = [threeD; ones(1,size(threeD,2))];
reproj1 = P1*threeD;
reproj2 = P2*threeD;
% make the last homogeneous coordinate 1
reproj1 = bsxfun(@rdivide, reproj1, reproj1(end,:));
reproj2 = bsxfun(@rdivide, reproj2, reproj2(end,:));


