close all;
clear all;

% TODO: get these helper functions a space of their own (e.g in a class)

%%%%%%%%%%%%%%%%%%%%%%%%
%% helper functions
%%%%%%%%%%%%%%%%%%%%%%%%

%% find the features in fb matching with fa
function [indices_map] = repeatedFeatures(fa, fb)
    % features of fb matching with fa (0 means no match)
    % indices_map(3) = 603 means fb(:,3) matches with fa(:,603)
    % indices_map(4) = 0 means fb(:,4) matches with no feature in fa
    
    indices_map = zeros(1,size(fb,2));
    for i = 1:size(fb,2)
        matching_indices = matchingIndices(fa(1:2,:), fb(1:2,i));
        if(length(matching_indices))
            indices_map(i) = matching_indices(1);
        else
            indices_map(i) = 0;
        end
    end
end

%% find the columns in M matching vector v
function [matching_indices] = matchingIndices(M,v)
    % if x and y are apart only by this threshold, we assume they match
    feature_match_threshold = .00000001;

    difference = bsxfun(@minus, M, v);
    matching = abs(difference)<feature_match_threshold;
    matching = matching(1,:) & matching(2,:);
    matching_indices = find(matching);
end

%Set some parameter first
ransac_round = 500;
thr = 4.0;

%% read the images
Im{1} = imread('../fountain-P11/0000.jpg');
Im{2} = imread('../fountain-P11/0001.jpg');
Im{3} = imread('../fountain-P11/0002.jpg');

%% resize image (apply if you are running out of memory)
% for i = 1:length(Im)
%   Im{i} = imresize(Im{i},.5);
% end

%% intrisic matrix (given)
K = [ ...
        1077.9  0       594.0
        0       1077.9  393.3
        0       0       1   ...
    ];

%% get corresponding features (in homogeneous frame)
[f{1}, f{2}] = getCorrespondingFeatures(Im{1}, Im{2});
f{1} = [f{1}; ones(1,size(f{1},2))];
f{2} = [f{2}; ones(1,size(f{2},2))];

%% compute the essential matrix (and extrinsic parameters)
[E, R, t, inliers]  = compute_E_ransac(f{1}, f{2}, ransac_round, K, K, thr);
% the only features I'm interest in are inliers
f{1} = f{1}(:,inliers);
f{2} = f{2}(:,inliers);
%% compute the fundamental matrix
F = inv(K)'*E*inv(K);

%% plot some epipolar lines
% plotEpipolars(Im{1}, Im{2}, f{1}(:,inliers), f{2}(:,inliers), F, 5);

% the first camera is supposed to be the reference frame
extrinsic1 = [eye(3) zeros(3,1)];
extrinsic2 = [R t];
P1 = K * extrinsic1;
P2 = K * extrinsic2;


%% finding x,y,z coordinates of corresponding features using triangulation
threeD = linearTriangulate(P1, P2, f{1}, f{2});
%% check reprojection error
threeD = [threeD; ones(1,size(threeD,2))];
reproj1 = P1*threeD;
reproj2 = P2*threeD;
% make the last homogeneous coordinate 1
reproj1 = bsxfun(@rdivide, reproj1, reproj1(end,:));
reproj2 = bsxfun(@rdivide, reproj2, reproj2(end,:));

%% getting the point cloud
% TODO: see if you can interpolate pixels instead of rounding
fa_pixels = round(f{1}(1:2,:));
fb_pixels = round(f{2}(1:2,:));

% ply_matrix has alternately x,y,z coordinates and RGB values for each point pair
%% writing only the inliers in ply file
% ply_matrix = zeros(length(inliers)*2, 3);
% for i = 1:length(inliers)
%     pixel1 = fa_pixels(:,inliers(i));
%     pixel2 = fb_pixels(:,inliers(i));
%     RGB = (double(Im{1}(pixel1(2),pixel1(1),:)) + double(Im{2}(pixel2(2),pixel2(1),:))) / 2.;
%     RGB = uint8(RGB);
%     ply_matrix(2*i-1, :) = threeD(:,inliers(i))';
%     ply_matrix(2*i, :) = [RGB(1,1,1) RGB(1,1,2) RGB(1,1,3)];
% end

%% writing all the corresponding points in ply file
ply_matrix = zeros(size(f{1},2)*2, 3);
for i = 1:size(f{1},2)
    pixel1 = fa_pixels(:,i);
    pixel2 = fb_pixels(:,i);
    RGB = (double(Im{1}(pixel1(2),pixel1(1),:)) + double(Im{2}(pixel2(2),pixel2(1),:))) / 2.;
    RGB = uint8(RGB);
    ply_matrix(2*i-1, :) = threeD(1:3,i)';
    ply_matrix(2*i, :) = [RGB(1,1,1) RGB(1,1,2) RGB(1,1,3)];
end
writePly('output.ply', ply_matrix);

% k is the image index (3,4,...)
k = 3; % make this change by loop
recent_images = 1; % take 2 recent images for correspondence
%TODO: dynamic resizing of matrix is expensive, try to do static
f{k} = []; 
% 3D points corresponding to features in f{k}
fk_threeD = [];
for l = 1:recent_images
    % we check with k-l images (l=1,2,..,recent_images)
    if l > k
        break;
    end
    % f1k -> features of (k-l)th image matching with kth image
    % fk1 -> features of kth image matching with (k-l)th image
    % 1 is the alias for (k-l)th image (previous images)
    [f1k, fk1] = getCorrespondingFeatures(Im{k-l}, Im{k});    
    f1k = [f1k; ones(1,size(f1k,2))];
    fk1 = [fk1; ones(1,size(fk1,2))];   
    indices_map = repeatedFeatures(f{k-l},f1k);
    matching_indices = find(indices_map);
    non_matching_indices = find(~indices_map);
    for i=1:length(matching_indices)
        feature_index_k = matching_indices(i);
        feature_index_1 = indices_map(feature_index_k);
        % if feature_index_k not present in f{k}, add it
        if(size(f{k},2)==0 || ~size(matchingIndices(f{k},fk1(:,feature_index_k)),2))
            f{k} = [f{k} fk1(:,feature_index_k)];
            % TODO: the threeD is associated with (k-l)th image is static, make it dynamic
            fk_threeD = [fk_threeD threeD(:,feature_index_1)];
        end
    end
end

%% compute the essential matrix (and extrinsic parameters)
[E3, R3, t3, inliers]  = compute_E_ransac(f1k, fk1, ransac_round, K, K, thr);

% transformation from 2 to 3
transformation23 = [R3 t3; zeros(1,3) 1];
% transformation from 1 to 2
transformation12 = [R t; zeros(1,3) 1];
% transformation from 1 to 3
transformation13 = transformation23 * transformation12;
% this is relative to the first image
extrinsic3 = [eye(3) zeros(3,1)] * transformation13;
P3 = K * extrinsic3;

%% finding x,y,z coordinates of corresponding features using triangulation
threeD2 = linearTriangulate(P2, P3, f1k, fk1);

fa_pixels = round(f1k);
fb_pixels = round(fk1);
%% writing all the corresponding points in ply file
ply_matrix = zeros(size(fk1,2)*2, 3);
for i = 1:size(f{1},2)
    pixel1 = fa_pixels(:,i);
    pixel2 = fb_pixels(:,i);
    RGB = (double(Im{2}(pixel1(2),pixel1(1),:)) + double(Im{3}(pixel2(2),pixel2(1),:))) / 2.;
    RGB = uint8(RGB);
    ply_matrix(2*i-1, :) = threeD(1:3,i)';
    ply_matrix(2*i, :) = [RGB(1,1,1) RGB(1,1,2) RGB(1,1,3)];
end
writePly('output2.ply', ply_matrix);

