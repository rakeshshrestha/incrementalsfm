%% this script just dumps only the unique features of the scene on point cloud (ply file)
close all;
clear all;

%%%%%%%%%%%%%%%%%%%%%%%%
%% helper functions
%%%%%%%%%%%%%%%%%%%%%%%%

%% find reprojection error (given camera matrices and 2D-3D points)
function [reproj_error] = reprojectionError(Pa, Pb, fa, fb, threeD)
    if(size(threeD,1)==3)
        threeD = [threeD; ones(1,size(threeD,2))];
    elseif(size(threeD,1)>4)
        threeD = threeD(1:4,:);
    end

    reproj_a = Pa * threeD;
    reproj_b = Pb * threeD;
    reproj_a = bsxfun(@rdivide, reproj_a, reproj_a(end,:));
    reproj_b = bsxfun(@rdivide, reproj_b, reproj_b(end,:));
    difference = [fa(1:2,:) - reproj_a(1:2,:); fb(1:2,:) - reproj_b(1:2,:)];
    reproj_error = sum(sum(difference .^ 2));
    
end

%% find the inliers among the features
function [inliers] = inliersToCamera(Pa, Pb, fa, fb)
    % variation upto 3 pixels allowed (in both x and y) to be considered inlier
    threshold = 3;

    threeD = linearTriangulate(Pa, Pb, fa, fb);
    threeD = [threeD; ones(1,size(threeD,2))];
    reproj_a = Pa * threeD;
    reproj_b = Pb * threeD;
    reproj_a = bsxfun(@rdivide, reproj_a, reproj_a(end,:));
    reproj_b = bsxfun(@rdivide, reproj_b, reproj_b(end,:));

    difference1 = abs(fa(1:2,:) - reproj_a(1:2,:));
    difference2 = abs(fb(1:2,:) - reproj_b(1:2,:));
    inliers1 = difference1 < threshold;
    inliers2 = difference2 < threshold;
    inliers = inliers1(1,:) & inliers1(2,:) & inliers2(1,:) & inliers2(2,:);
    inliers = find(inliers);
                 
end

%% find the features in fb matching with fa
function [indices_map] = repeatedFeatures(fa, fb)
    % features of fb matching with fa (0 means no match)
    % indices_map(3) = 603 means fb(:,3) matches with fa(:,603)
    % indices_map(4) = 0 means fb(:,4) matches with no feature in fa
    
    indices_map = zeros(1,size(fb,2));
    for i = 1:size(fb,2)
        matching_indices = matchingIndices(fa(1:2,:), fb(1:2,i));
        if(length(matching_indices))
            indices_map(i) = matching_indices(1);
        else
            indices_map(i) = 0;
        end
    end
end

%% find the columns in M matching vector v
function [matching_indices] = matchingIndices(M,v)
    % if x and y are apart only by this threshold, we assume they match
    feature_match_threshold = .00000001;

    difference = bsxfun(@minus, M, v);
    matching = abs(difference)<feature_match_threshold;
    matching = matching(1,:) & matching(2,:);
    matching_indices = find(matching);
end

%Set some parameter first
ransac_round = 500;
thr = 4.0;
% number of past images to check for new features
no_of_past_images = 2;

%% read the images
Im{1} = imread('../fountain-P11/0000.jpg');
Im{2} = imread('../fountain-P11/0001.jpg');
% Im{3} = imread('../fountain-P11/0002.jpg');
% Im{4} = imread('../fountain-P11/0003.jpg');
% Im{5} = imread('../fountain-P11/0004.jpg');
% Im{6} = imread('../fountain-P11/0005.jpg');
% Im{7} = imread('../fountain-P11/0006.jpg');
% Im{8} = imread('../fountain-P11/0007.jpg');
% Im{9} = imread('../fountain-P11/0008.jpg');
% Im{10} = imread('../fountain-P11/0009.jpg');
% Im{11} = imread('../fountain-P11/0010.jpg');


%% resize image (apply if you are running out of memory)
% for i = 1:length(Im)
%   Im{i} = imresize(Im{i},.5);
% end

%% intrisic matrix (given)
K = [ ...
        1077.9  0       594.0
        0       1077.9  393.3
        0       0       1   ...
    ];

% this is transformation from image frame to world frame
% the first image frame is considered as world frame
transformation{1} = [eye(3) zeros(3,1);zeros(1,3) 1];

% the camera matrix for different camera (first camera is reference)
P{1} = K * [eye(3) zeros(3,1)];% equivalent to K * [I|0] * transformation{1}

% this matrix contains point location in space and corresponding RGB values in alternating rows
ply_matrix = [];

% unique_features{k}{l}->4XN matrix, 
% Corresponding features of image k with respect to previous (k-l) image stacked vertically
% these features are not present in other indices of unique_features (theoritically), hence unique
unique_features{1} = []; % image 1 has no image previous to it
unique_features{2}{1} = []; % unique features of image 2 corresponding to image 2-1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TODO: we don't need to know from which previous image we got the feature and the features in previous image matching the current image
% eg. unique_features{2} = [...] may contain features from any previous image
% for triangulating the 3D coordinates, we may store previous image's corresponding features in different matrix which may be overwritten later
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% point_cloud{k}{l} -> 3D coordinates of corresponding unique_features between images k and k-l
% 3XN matrix - 3D coordinates corresponding to the unique_features
point_cloud{1} = []; % image 1 has no image previous to it
point_cloud{2}{1} = [];

% rgb_stack{k}{l}{m} -> nX3 matrix (n is the number of overlapping pixel RGB values)
% rgb_stack{k}{l}{m} -> RGB values from all images of the mth unique corresponding feature between images k and k-l
% may come from more than 2 images (if they are the same feature)
rgb_stack{1} = 0;
rgb_stack{2}{1}{1} = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TODO: implement the contributors in the main loop
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% contributors{k}{l} -> images that contributed to the unique_features{k}{l}
% TODO: Images k and k-l are always there. Redundant! Remove the l cell index marking
contributors{1} = [];
contributors{2}{1} = [];

%% TODO: find the correspondence between first two images in advance and put those features as unique features
%% get corresponding features between current and previous image (in homogeneous frame)
[fa, fb] = getCorrespondingFeatures(Im{1}, Im{2});
fa = [fa; ones(1,size(fa,2))];
fb = [fb; ones(1,size(fb,2))];

%% compute the essential matrix (and extrinsic parameters)
[E, R, t, inliers]  = compute_E_ransac(fa, fb, ransac_round, K, K, thr);
% the only features I'm interest in plotting are inliers
fa = fa(:,inliers);
fb = fb(:,inliers);
% for the first pair of images, all the features are unique
unique_features{2}{1} = [fa(1:2,:); fb(1:2,:)];
% finding transformations to world (first image) frame
extrinsic2 = [R t];
P{2} = K * extrinsic2;
transformation{2} = [R t; zeros(1,3) 1];
%% finding x,y,z coordinates of corresponding features using triangulation
threeD = linearTriangulate(P{1}, P{2}, fa, fb);
%% minimize least square error
errorFunction = @(x) reprojectionError(P{1},P{2},fa,fb,reshape(x,[3,size(fa,2)]));
fprintf('previous error: %f\n',reprojectionError(P{1},P{2},fa,fb,threeD));
threeD = smarquardt(errorFunction, reshape(threeD, [1,3*size(fa,2)]));
fprintf('current error: %f\n',errorFunction(threeD));
threeD = reshape(threeD,[3,size(fa,2)]);

point_cloud{2}{1} = threeD(1:3,:);
for i=1:size(fa,2)
    pixel1 = round(unique_features{2}{1}(1:2,i));
    pixel2 = round(unique_features{2}{1}(3:4,i));
    RGB1 = Im{1}(pixel1(2),pixel1(1),:);
    RGB2 = Im{2}(pixel2(2),pixel2(1),:);
    rgb_stack{2}{1}{i} = [RGB1(1,1,1) RGB1(1,1,2) RGB1(1,1,3);
                                    RGB2(1,1,1) RGB2(1,1,2) RGB2(1,1,3)];
end

for image_index=3:length(Im)
    fprintf('image %d:\n',image_index);
    %% get corresponding features between current and previous image (in homogeneous frame)
    [fa, fb] = getCorrespondingFeatures(Im{image_index-1}, Im{image_index});
    fa = [fa; ones(1,size(fa,2))];
    fb = [fb; ones(1,size(fb,2))];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    % TODO: find the camera matrix by known 3D to 2D mapping, not essential matrix!
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

    %% compute the essential matrix (and extrinsic parameters)
    [E, R, t, inliers]  = compute_E_ransac(fa, fb, ransac_round, K, K, thr);
    % the only features I'm interest in plotting are inliers
    fa = fa(:,inliers);
    fb = fb(:,inliers);
        
    % transformation from image_index-1 frame to image_frame
    T = [R t; zeros(1,3) 1];
    % transformation from world frame to image frame
    transformation{image_index} = T * transformation{image_index-1};
    % P = K * [I|0] * transformation
    P{image_index} = K * [eye(3) zeros(3,1)] * transformation{image_index};
    
    for l = 1:no_of_past_images
        % we check with image_index-l images (l=1,2,..,no_of_past_images)
        if l >= image_index
            break;
        end
        fprintf('Past image: %d\n',l);
        if(l>1)
            % we haven't found correspondence the image_index and image_index-l(where l>1)
            [fa, fb] = getCorrespondingFeatures(Im{image_index-l}, Im{image_index});
            fa = [fa; ones(1,size(fa,2))];
            fb = [fb; ones(1,size(fb,2))];

            inliers = inliersToCamera(P{image_index-l}, P{image_index}, fa, fb);
            fprintf('inliers: %d/%d\n',length(inliers), size(fa,2));
            % the only features I'm interest in plotting are inliers
            fa = fa(:,inliers);
            fb = fb(:,inliers);
        end
        %% add points of image_index to the cloud corresponding to image_index-l
        
        % find past unique features of image_index-l corresponding with image_index
        non_matching_indices = [];
        matching_indices = [];
        unique_features{image_index}{l} = [];

        if(image_index-l == 1)
            % this is when the previous image is image no. 1
            % we need to check the matching featurers with image 2 in that case
            
            past_features = unique_features{2}{1}(1:2,:);
            past_features = [past_features; ones(1,size(past_features,2))];
            indices_map = repeatedFeatures(past_features,fa);
            matching_indices = find(indices_map);
            non_matching_indices = find(~indices_map);
               
        else
            for i=1:length(unique_features{image_index-l})
                past_features = unique_features{image_index-l}{i}(3:4,:);
                past_features = [past_features; ones(1,size(past_features,2))];
                indices_map = repeatedFeatures(past_features,fa);
                matching_indices_now = find(indices_map);
                non_matching_indices_now = find(~indices_map);

                for j = matching_indices_now
                    %add only the indices that are not already present
                    if(~length(find(matching_indices==j)))
                        matching_indices = [matching_indices j];
                    end
                end
                for j = non_matching_indices_now
                    %add only the indices that are not already present
                    if(~length(find(non_matching_indices==j)))
                        non_matching_indices = [non_matching_indices j];
                    end
                end
                
                % add the RGB value of the matching feature to old rgb_stack (for later averaging)
                for j=1:length(matching_indices_now)
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % TODO: use the known 3D to 2D correspondence from here to find camera matrix
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    feature_index_present = matching_indices_now(j);
                    feature_index_previous = indices_map(feature_index_present);
                    pixel = round(fb(:,feature_index_present));
                    RGB = Im{image_index}(pixel(2), pixel(1),:);
                    rgb_stack{image_index-l}{i}{feature_index_previous} = [...
                                                                    rgb_stack{image_index-l}{i}{feature_index_previous};
                                                                    RGB(1,1,1) RGB(1,1,2) RGB(1,1,3)
                                                                    ];  
                end
            end
        end
                
        non_matching_indices_clean=[];
        for j=non_matching_indices
            %add only the indices that are not present in matching_indices
            if(~length(find(matching_indices==j)))
                non_matching_indices_clean = [non_matching_indices_clean j];
            end
        end

        fprintf('new features added: %d\n',size(non_matching_indices_clean,2));
        fprintf('repeated features: %d\n',size(matching_indices,2));

        % add non matching feature (i.e unique feature)
        unique_features{image_index}{l} = [...
                                            fa(1:2,non_matching_indices_clean);
                                            fb(1:2,non_matching_indices_clean)
                                        ];

        no_of_unique_features = size(unique_features{image_index}{l},2);
        %% finding x,y,z coordinates of corresponding features using triangulation
        threeD = linearTriangulate( P{image_index-l}, 
                                    P{image_index}, 
                                    [unique_features{image_index}{l}(1:2,:); ones(1,no_of_unique_features)],
                                    [unique_features{image_index}{l}(3:4,:); ones(1,no_of_unique_features)]
                                );

        point_cloud{image_index}{l} = threeD(1:3,:);
        for i=1:no_of_unique_features
            pixel1 = round(unique_features{image_index}{l}(1:2,i));
            pixel2 = round(unique_features{image_index}{l}(3:4,i));
            RGB1 = Im{image_index-l}(pixel1(2),pixel1(1),:);
            RGB2 = Im{image_index}(pixel2(2),pixel2(1),:);
            rgb_stack{image_index}{l}{i} = [RGB1(1,1,1) RGB1(1,1,2) RGB1(1,1,3);
                                            RGB2(1,1,1) RGB2(1,1,2) RGB2(1,1,3)];
        end
    end
    fprintf('\n');
end

%% write the point cloud from each image pair to ply file
no_of_points_in_cloud = 0;
for image_index=2:length(Im)
    for l=1:size(unique_features{image_index},2)
        no_of_points_in_cloud = no_of_points_in_cloud + size(unique_features{image_index}{l},2);
    end
end

% this matrix contains point location in space and corresponding RGB values in alternating rows
ply_matrix = zeros(2*no_of_points_in_cloud,3);
point_count = 1;
for image_index=2:length(Im)
    for l=1:size(unique_features{image_index},2)
        for i = 1:size(unique_features{image_index}{l},2)
            RGB = round(mean(rgb_stack{image_index}{l}{i}));
            ply_matrix(2*point_count-1,:) = point_cloud{image_index}{l}(:,i)';
            ply_matrix(2*point_count,:) = RGB;
            point_count = point_count + 1;
        end
    end
end

writePly('output.ply', ply_matrix);