%% this script just dumps only the unique features of the scene on point cloud (ply file)
close all;
clear all;


%Set some parameter first
ransac_round = 500;
thr = 4.0;
% number of past images to check for new features
no_of_past_images = 2;

%% read the images
Im{1} = imread('../fountain-P11/0000.jpg');
Im{2} = imread('../fountain-P11/0001.jpg');
% Im{3} = imread('../fountain-P11/0002.jpg');
% Im{4} = imread('../fountain-P11/0003.jpg');
% Im{5} = imread('../fountain-P11/0004.jpg');
% Im{6} = imread('../fountain-P11/0005.jpg');
% Im{7} = imread('../fountain-P11/0006.jpg');
% Im{8} = imread('../fountain-P11/0007.jpg');
% Im{9} = imread('../fountain-P11/0008.jpg');
% Im{10} = imread('../fountain-P11/0009.jpg');
% Im{11} = imread('../fountain-P11/0010.jpg');

%% resize image (apply if you are running out of memory)
% for i = 1:length(Im)
%   Im{i} = imresize(Im{i},.5);
% end

%% intrisic matrix (given)
K = [ ...
        1077.9  0       594.0
        0       1077.9  393.3
        0       0       1   ...
    ];

% this is transformation from image frame to world frame
% the first image frame is considered as world frame
transformation{1} = [eye(3) zeros(3,1);zeros(1,3) 1];

% the camera matrix for different camera (first camera is reference)
P{1} = K * [eye(3) zeros(3,1)];% equivalent to K * [I|0] * transformation{1}

% this matrix contains point location in space and corresponding RGB values in alternating rows
ply_matrix = [];

% unique_features{k}-> 2Xm matrix (m is the number of images that corresponds to 3D point k)
% coordinates of features from different images projected from point k in the cloud
unique_features{1} = [];

% point_cloud{k} -> 3D coordinates of kth point in the cloud
% 3XN matrix - N is the number of point in cloud
point_cloud{1} = []; 

% contributors{k} -> images that contributed to the kth point in the cloud
contributors{1} = [];

no_of_points_in_cloud = 0;

%% get corresponding features between current and previous image (in homogeneous frame)
[fa, fb] = getCorrespondingFeatures(Im{1}, Im{2});
fa = [fa; ones(1,size(fa,2))];
fb = [fb; ones(1,size(fb,2))];
% convert to standard frame
fa_std = featuresToStandardFrame(fa, size(Im{1}));
fb_std = featuresToStandardFrame(fb, size(Im{2}));

%% compute the essential matrix (and extrinsic parameters)
[E, R, t, inliers]  = compute_E_ransac(fa_std, fb_std, ransac_round, K, K, thr);
% the only features I'm interest in plotting are inliers
fa = fa(:,inliers); fa_std = fa_std(:,inliers);
fb = fb(:,inliers); fb_std = fb_std(:,inliers);
% finding transformations to world (first image) frame
extrinsic2 = [R t];
P{2} = K * extrinsic2;
transformation{2} = [R t; zeros(1,3) 1];


%% finding x,y,z coordinates of corresponding features using triangulation
threeD = linearTriangulate(P{1}, P{2}, fa_std, fb_std);
% for the first pair of images, all the features are unique
for i= 1:size(fa,2)
    no_of_points_in_cloud = no_of_points_in_cloud + 1;
    point_cloud{no_of_points_in_cloud} = threeD(:,i);
    unique_features{no_of_points_in_cloud} = [fa(1:2,i), fb(1:2,i)];
    contributors{no_of_points_in_cloud} = [1 2];
end

%% generating BAL file
%writeBal('bal.txt', point_cloud, unique_features, contributors, transformation, K, 2);


for image_index=3:length(Im)
    fprintf('image %d:\n',image_index);
    %% get corresponding features between current and previous image (in homogeneous frame)
    [fa, fb] = getCorrespondingFeatures(Im{image_index-1}, Im{image_index});
    fa = [fa; ones(1,size(fa,2))];
    fb = [fb; ones(1,size(fb,2))];
    % convert to standard frame
    fa_std = featuresToStandardFrame(fa, size(Im{image_index-1}));
    fb_std = featuresToStandardFrame(fb, size(Im{image_index}));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    % TODO: find the camera matrix by known 3D to 2D mapping, not essential matrix!
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

    %% compute the essential matrix (and extrinsic parameters)
    [E, R, t, inliers]  = compute_E_ransac(fa_std, fb_std, ransac_round, K, K, thr);
    % the only features I'm interest in plotting are inliers
    fa = fa(:,inliers); fa_std = fa_std(:,inliers);
    fb = fb(:,inliers); fb_std = fb_std(:,inliers);
        
    % transformation from image_index-1 frame to image_frame
    T = [R t; zeros(1,3) 1];
    % transformation from world frame to image frame
    transformation{image_index} = T * transformation{image_index-1};
    % P = K * [I|0] * transformation
    P{image_index} = K * [eye(3) zeros(3,1)] * transformation{image_index};


    for l = 1:no_of_past_images
        % we check with image_index-l images (l=1,2,..,no_of_past_images)
        if l >= image_index
            break;
        end
        fprintf('Past image: %d\n',l);
        if(l>1)
            % we haven't found correspondence the image_index and image_index-l(where l>1)
            [fa, fb] = getCorrespondingFeatures(Im{image_index-l}, Im{image_index});
            fa = [fa; ones(1,size(fa,2))];
            fb = [fb; ones(1,size(fb,2))];
            % convert to standard frame
            fa_std = featuresToStandardFrame(fa, size(Im{image_index-l}));
            fb_std = featuresToStandardFrame(fb, size(Im{image_index}));

            inliers = inliersToCamera(P{image_index-l}, P{image_index}, fa_std, fb_std);
            fprintf('inliers: %d/%d\n',length(inliers), size(fa,2));
            % the only features I'm interest in plotting are inliers
            fa = fa(:,inliers); fa_std = fa_std(:,inliers);
            fb = fb(:,inliers); fb_std = fb_std(:,inliers);
        end
        
        %% add points of image_index to the cloud corresponding to image_index-l
        
        % find past unique features of image_index-l corresponding with image_index
        is_matching = zeros(1,size(fa,2));
                
        % checking already plotted points
        for i=1:length(point_cloud)
            % this is the order number of the image among the contributors to the point
            k = find(contributors{i}==(image_index-l)); 
            % we look at only the points that were projected by image_index-l and that have not already been contributed by image_index
            if(~length(k) || length(find(contributors{i}==image_index)))
                continue;
            end
            k = k(1);

            past_feature = unique_features{i}(:,k);
            matching_index_now = matchingIndices(fa,[past_feature; 1]);
            if(length(matching_index_now))
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % TODO: use the known 3D to 2D mapping from here to estimate camera matrix
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                matching_index_now = matching_index_now(1);
                is_matching(matching_index_now) = 1;
                contributors{i} = [contributors{i} image_index];
                unique_features{i} = [unique_features{i} fb(1:2,matching_index_now)];
            end
        end
                
        non_matching_indices= find(~is_matching);
        
        fprintf('new features added: %d\n',size(non_matching_indices,2));
        fprintf('repeated features: %d\n',size(fa,2) - size(non_matching_indices,2));
        
        no_of_unique_features = size(non_matching_indices,2);
        %% finding x,y,z coordinates of corresponding features using triangulation
        threeD = linearTriangulate( P{image_index-l}, ...
                                    P{image_index}, ...
                                    fa_std(:,non_matching_indices), ...
                                    fb_std(:,non_matching_indices)
                                );
        
        for i=1:length(non_matching_indices)
            no_of_points_in_cloud = no_of_points_in_cloud + 1;
            feature_index = non_matching_indices(i);
            point_cloud{no_of_points_in_cloud} = threeD(:,i);
            unique_features{no_of_points_in_cloud} = [fa(1:2,feature_index), fb(1:2,feature_index)];
            contributors{no_of_points_in_cloud} = [image_index-l image_index];
        end
    end
    fprintf('\n');
end

%% triangulating from features of all images that contributed to a point
%% no need, this will be managed by bundle adjustment
% for i = 1:length(point_cloud)
%     if(length(contributors{i})<=2), continue; end % already done
%     P_all = zeros(length(contributors{i})*3, 4);
%     f_all = zeros(3, length(contributors{i}));
%     for j = 1:length(contributors{i})
%         P_all(3*j-2:3*j, :) = P{contributors{i}(j)};
%         %f_all(:,j) = [unique_features{i}(:,j); 1];
%         % convert to standard frame
%         f_all(:,j) = featuresToStandardFrame([unique_features{i}(:,j); 1], size(Im{contributors{i}(j)}));
%     end
    
%     point_cloud{i} = linearTriangulateMultiple(P_all, f_all);
% end

% write ply
ply_matrix = writePly('ply/output.ply', Im, point_cloud, unique_features, contributors);

%% generating BAL file
writeBal('ceres/bal.txt', point_cloud, unique_features, contributors, transformation, K, length(Im));

