function [reproj_error_vector] = reprojectionErrorVector(threeD, unique_features, contributors, P)
    no_of_error_elements = 0;
    for i=1:length(contributors)
        no_of_error_elements = no_of_error_elements + length(contributors{i});
    end
    reproj_error_vector = zeros(no_of_error_elements,1);
    error_count = 1;
    for i = 1:length(unique_features)
        contributing_images = contributors{i};
        for j = 1:length(contributing_images)
            image_index = contributing_images(j);
            feature = unique_features{i}(:,j);
            projection = P{image_index} * [threeD(:,i); 1];
            projection = projection(1:2,:) / projection(3,:);
            reproj_error = sum(sum((feature - projection).^2));
            reproj_error_vector(error_count) = reproj_error;
            error_count = error_count + 1;
        end
    end
end
