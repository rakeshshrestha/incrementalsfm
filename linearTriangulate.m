%% finding x,y,z coordinates of corresponding features using triangulation
function [threeD] = linearTriangulate(P1, P2, fa, fb)
	% @param P1, P2: camera matrix for two images
	% @param fa, fb: corresponding points in two images
	% @output threeD: 3D coordinates of corresponding features

	if(size(P1) ~= size(P2))
		error('size of P1 and P2 must be same');
	end
	if(size(P1) ~= [3,4])
		error('size of P1 and P2 must be 3X4');
	end
	if(size(fa) ~= size(fb))
		error('size of fa and fb must be same');
	end
	if(size(fa,1) ~= 3)
		error('fa and fb must be in homogeneous form [wx;wy;w]');
	end
	% convert to form [x,y,1]
	fa = bsxfun(@rdivide, fa, fa(end,:));
	fb = bsxfun(@rdivide, fb, fb(end,:));
	
	threeD = zeros(3,size(fa,2));
	for i=1:size(fa,2)
	    x = fa(1:2,i);
	    xp = fb(1:2,i);
	    A = [...
	            P1(3,:)*x(1) - P1(1,:);
	            P1(3,:)*x(2) - P1(2,:);
	            P2(3,:)*xp(1) - P2(1,:);
	            P2(3,:)*xp(2) - P2(2,:)...
	        ];

	    [~,~,V] = svd(A);
	    threeD(:,i) = V(1:3,end)/V(4,end);

	    % only y, xp, yp are correct (x is not correct)
	    % A = A(2:4,:);
	    % threeD(:,i) = inv(A(:,1:3)) * -A(:,4);
	end
end
